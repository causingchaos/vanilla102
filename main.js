//()() iffy function.
;(function(window){  // add ; in the event the code is minimized, and proceeding code 
                                            //doesn't start with ;
    // Game
    var Game = function(el, option){ //element passed in, option is the type of game
        
        this.el = document.getElementById(el);
        this.option = option;
        //     Info Section

        this.info_div = document.createElement('div');
        this.info_div.id = "info_div";
        //     Deck
        
        this.deck_div = document.createElement("div");
        this.deck_div.id = "deck_div";
        this.gameDeck = new Deck(this.deck_div, option);
        this.gameDeck.buildDeck(); // Build deck

        //     Discard Pile
        //     Rules

        this.el.appendChild(this.info_div); //append to DOM
        this.el.appendChild(this.deck_div); //append to DOM
    }

    // Deck
    var Deck = function(deck_div, options){
        this.deckData = option.data;
        this.buildDeck = function() {
            var parentFrag = document.createDocumentFragment(); //this will store our cards
            deck_div.innerHTML = "";
            for (var i = this.deckData.length - 1; i >= 0; i--){
                var card = new Card();
                card.id = "card-" + i;
                card.data = this.deckData[i]
                card.buildCard(parentFrag);
            }
            deck_div.appendChild(parentFrag) //this has all our cards, append to virtual dom
        }
        

    }
    //     Cards
    //     ---
    //     shuffle
    //     stack

    // Cards
    var Cards = function(){
        this.id = "";
        this.data = "";
        this.cardCont = document.createElement("div");
        this.cardCont.className = "card_container";
        this.cardFront = document.createElement("div");
        this.cardFront.className = "card_front";
        this.cardBack = document.createElement("div");
        this.cardBack.className = "card_back";
        this.buildCard = function() {
            var flipDiv = document.createElement("div"),
                frontValDiv = document.createElement("div"),
                backValDiv = document.createElement("div"),
                catDiv = document.createElement("div")
            flipDiv.className = "flip";
            frontValDiv.className = "front_val";
            backValDiv.className = "back_val";
            catDiv.className = "cat_val";

            frontValDiv.innerHTML = this.data.q; //flashcard q
            backValDiv.innerHTML = this.data.a; //flashcard a
            catDiv.innerHTML = this.data.category;

            this.cardFront.appendChild(frontValDiv);
            this.cardFront.appendChild(catDiv);
            this.cardBack.appendChild(backValDiv);

            flipDiv.appendChild(this.cardFront);
            flipDiv.appendChild(this.cardBack);

            this.cardCont.id = this.id;
            this.cardCont.appendChild(flipDiv);
            parentFrag.appendChild(this.cardCont)

        }
    }
    //     Val
    //     suit 
    //     -----
    //     flip 

    // Discard Pile
    var Discard Pile = function() {

    }
    //     Holders
    //     ---
    //     accept or reject
    window.Game = Game;
})(window);
